#include "unity.h"
#include "c_queue.h"

queue_S q1;
queue_S *queue=&q1;
uint8_t pop_value;

void test_queue__init(void){
	queue__init(queue);
	TEST_ASSERT_EQUAL_UINT8(0, queue->queue_size) ;
	TEST_ASSERT_EQUAL_UINT8(0, queue->first_in_queue);
	TEST_ASSERT_EQUAL_UINT8(-1, queue->last_in_queue);
}


void test_queue__push(void){

	for(int i=1;i<=100;i++){
		TEST_ASSERT_TRUE(queue__push(queue,i));
		TEST_ASSERT_EQUAL_UINT8(i, queue__get_count(queue));
	}
	TEST_ASSERT_FALSE(queue__push(queue,101));
}

void test_queue__pop(void){
	for(int i=1,j=100;i<=100;i++,j--){
		TEST_ASSERT_EQUAL_UINT8(j, queue__get_count(queue));
		TEST_ASSERT_TRUE(queue__pop(queue,&pop_value));
		TEST_ASSERT_EQUAL_UINT8(i,pop_value);
		//TEST_ASSERT_EQUAL_UINT8(j, queue__get_count(queue));

	}
	TEST_ASSERT_FALSE(queue__pop(queue,&pop_value));
	for(int i=1;i<=100;i++){
		TEST_ASSERT_TRUE(queue__push(queue,i));
		TEST_ASSERT_EQUAL_UINT8(i, queue__get_count(queue));
	}
}

void test_push_pop(void){
	queue__init(queue);
	for(int i=1;i<=100;i++){
		TEST_ASSERT_TRUE(queue__push(queue,i));
		TEST_ASSERT_EQUAL_UINT8(i, queue__get_count(queue));
	}
	for(int i=1,j=100;i<=100;i++,j--){
		TEST_ASSERT_EQUAL_UINT8(j, queue__get_count(queue));
		TEST_ASSERT_TRUE(queue__pop(queue,&pop_value));
		TEST_ASSERT_EQUAL_UINT8(i,pop_value);
	}
	for(int i=1;i<=10;i++){
		TEST_ASSERT_TRUE(queue__push(queue,i));
		TEST_ASSERT_EQUAL_UINT8(i, queue__get_count(queue));
	}

}

