// @file queue.h
#include <c_queue.h>


void queue__init(queue_S *queue){
	queue->queue_size=0;
	queue->first_in_queue=0;
	queue->last_in_queue=-1;

}



bool queue__push(queue_S *queue, uint8_t push_value){
	if((queue->queue_size) < SIZE_OF_ARRAY){
		++(queue->last_in_queue);		
		queue->queue_memory[(queue->last_in_queue)%SIZE_OF_ARRAY]=push_value;
		++(queue->queue_size);
		return true;
	}
	else{
		return false;
	}
}

bool queue__pop(queue_S *queue, uint8_t *pop_value){
	if((queue->queue_size)> 0){

		*pop_value=queue->queue_memory[(queue->first_in_queue)% SIZE_OF_ARRAY];//6
		++(queue->first_in_queue);
		--(queue->queue_size);
		return true;

	}
	else{
		return false;
	}
}

size_t queue__get_count(queue_S *queue){

	return (queue->queue_size);
}
