// @file queue.h
#include <stdint.h>
#include <stdbool.h>
#define SIZE_OF_ARRAY 100
typedef struct {
 uint8_t queue_memory[SIZE_OF_ARRAY];
 uint8_t queue_size;
 uint8_t first_in_queue;
 int8_t last_in_queue;


  // TODO: Add more members as needed
} queue_S;



void queue__init(queue_S *queue);
bool queue__push(queue_S *queue, uint8_t push_value);
bool queue__pop(queue_S *queue, uint8_t *pop_value);

size_t queue__get_count(queue_S *queue);
