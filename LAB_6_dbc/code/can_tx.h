/*
 * can_homework.h
 *
 *  Created on: Mar 4, 2019
 *      Author: Anurag
 */

#ifndef CAN_TX_H_
#define CAN_TX_H_

#include "can.h"
#include "generated_can.h"
bool check_switch_pressed(void);
void can_app_init_tx(void);
void can_app_tx(dbc_msg_hdr_t *msg_hdr,can_msg_t *txmsg);
void check_can_bus_off_tx(void);
void can_app_rx(void);
void can_prepare_txmsg(can_msg_t *txmsg);

#endif /* CAN_TX_H_ */
