/*
 * can_homework.c
 *
 *  Created on: Mar 4, 2019
 *      Author: Anurag
 */
#include <stdio.h>
#include "can_tx.h"
#include "gpio_wrapper.h"
//#include "FreeRTOS.h"
//#include "generated_can.h"

void check_can_bus_off_tx(void){
    if(CAN_is_bus_off(can1))
    CAN_reset_bus(can1);
    else
        return;
}
 void can_app_init_tx(void){
     CAN_init(can1,100,0,1,NULL,NULL);
     CAN_bypass_filter_accept_all_msgs();
     CAN_reset_bus(can1);
}
bool check_switch_pressed(void){
    return read_pin();
}

 void can_app_tx(dbc_msg_hdr_t *msg_hdr,can_msg_t *txmsg){
    can_msg_t temp={0};
     (*txmsg) = temp;
     SWITCH_CMD_t sensor_msg = {0};

     sensor_msg.SWITCH_Status = 0x01;
     sensor_msg.LIGHT_Sensor_Value = get_light_sensor_value();
     *msg_hdr = dbc_encode_SWITCH_CMD(txmsg->data.bytes,&sensor_msg);
     txmsg->msg_id = msg_hdr->mid;
     txmsg->frame_fields.data_len = msg_hdr->dlc;

     CAN_tx(can1, txmsg, 0);
     led_on();

}

