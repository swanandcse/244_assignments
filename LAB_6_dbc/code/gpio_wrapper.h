/*
 * gpio_wrapper.h
 *
 *  Created on: 04-Mar-2019
 *      Author: Kailash Chakravarty
 */

#ifndef GPIO_WRAPPER_H_
#define GPIO_WRAPPER_H_

#ifdef __cplusplus
extern "C" {
#endif
#include<stdbool.h>
#include <stdint.h>
	void setgpioasip(void);
	bool gpio_read(void);
	void TurnOFF_LED(void);
	void TurnON_LED(void);
	void set_as_input(void);
	bool read_pin(void);
	void led_on(void);
	void led_off(void);
	uint8_t get_light_sensor_value(void);
	void display_LED(char value);

#endif /* GPIO_WRAPPER_H_ */
#ifdef __cplusplus
}
#endif
