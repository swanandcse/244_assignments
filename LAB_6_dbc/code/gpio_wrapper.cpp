/*
 * gpio_wrapper.cpp
 *
 *  Created on: 04-Mar-2019
 *      Author: Kailash Chakravarty
 */


#include "gpio.hpp"
#include "io.hpp"
#include "gpio_wrapper.h"

GPIO myInput(P2_0);

//nandini
GPIO switch_ip(P1_9);

void set_as_input(void){
    switch_ip.setAsInput();
}
bool read_pin(void){
   return switch_ip.read();
}
void led_on(void){
    LE.on(1);
}
void led_off(void){
    LE.off(1);
}

uint8_t get_light_sensor_value(void){
    return LS.getPercentValue();
}

void display_LED(char value){
    LD.setNumber(value);
}

//nandini

void TurnON_LED(void)
{
    LE.on(1);
}

void TurnOFF_LED(void)
{
    LE.off(1);
}


void setgpioasip(void)
{
    myInput.setAsInput();
}

bool gpio_read(void)
{
    return myInput.read();
}
