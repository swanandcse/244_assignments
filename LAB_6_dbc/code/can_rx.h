/*
 * can_rx.h
 *
 *  Created on: Mar 6, 2019
 *      Author: Anurag
 */

#ifndef CAN_RX_H_
#define CAN_RX_H_
#include <stdint.h>
#include <stdbool.h>
#include "can.h"
#include "generated_can.h"
void check_can_bus_off_rx(void);
void can_app_rx(dbc_msg_hdr_t *msg_hdr,can_msg_t *txmsg);
bool check_valid_msg(can_msg_t *txmsg);

#endif /* CAN_RX_H_ */
