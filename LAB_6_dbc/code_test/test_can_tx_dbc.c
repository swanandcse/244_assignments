#include "unity.h"
#include "can_tx.h"
//#include "generated_can.h"
#include "Mockcan.h"
#include "Mockgpio_wrapper.h"



void test_chkBussoff_tx(void)
{
	CAN_is_bus_off_ExpectAndReturn(can1,true);	
	CAN_reset_bus_Expect(can1);
	check_can_bus_off_tx();

	CAN_is_bus_off_ExpectAndReturn(can1,false);   
	check_can_bus_off_tx();
	
}


void test_can_app_init_tx(void)
{
	//setgpioasip_Expect();
	CAN_init_ExpectAndReturn(can1,100,0,1,NULL,NULL,true);
	CAN_bypass_filter_accept_all_msgs_Expect();
	CAN_reset_bus_Expect(can1);
	can_app_init_tx();
	
	
}
void test_can_app_tx(void)
{
	
	can_msg_t txmsg = {0};
   //SWITCH_CMD_t sensor_msg = {0};
   //sensor_msg.SWITCH_Status = 0x01;
	dbc_msg_hdr_t msg_hdr;
	get_light_sensor_value_ExpectAndReturn(90);
	CAN_tx_ExpectAndReturn(can1, &txmsg, 0,true);
	led_on_Expect();
	can_app_tx(&msg_hdr,&txmsg);

	TEST_ASSERT_EQUAL_UINT32(txmsg.msg_id,msg_hdr.mid);
	TEST_ASSERT_EQUAL_UINT32(txmsg.frame_fields.data_len,msg_hdr.dlc);
   //txmsg.frame_fields.data_len = msg_hdr.dlc;



	
	
}
